package com.dev.template.service;

import com.dev.template.details.UserDetailsImpl;
import com.dev.template.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.UnsupportedEncodingException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class JwtServiceTest {

	@MockBean
	private JwtService jwtService;

	private final UserDetailsImpl userDetails =  new UserDetailsImpl(new User());

	@Test
	public void generateToken() throws UnsupportedEncodingException {
		when(jwtService.generateToken(any(User.class)))
				.thenReturn("token")
				.thenThrow(UnsupportedEncodingException.class);
		String expected = "token";
		assertEquals(expected, jwtService.generateToken(new User()));
	}

	@Test
	public void getUserDetails() {
		when(jwtService.getUserDetails(anyString())).thenReturn(userDetails);
		UserDetailsImpl expected = userDetails;
		assertEquals(expected, jwtService.getUserDetails("token"));
	}
}