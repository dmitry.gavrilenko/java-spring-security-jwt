package com.dev.template.service;

import com.dev.template.dto.SignInDTO;
import com.dev.template.dto.SignUpDTO;
import com.dev.template.exception.AuthorizeException;
import com.dev.template.model.User;
import com.dev.template.repository.UserRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.UnsupportedEncodingException;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class UserServiceImplTest {

	@MockBean
	private UserRepository userRepository;

	@MockBean
	private PasswordEncoder passwordEncoder;

	@MockBean
	private JwtService jwtService;


	@Test
	public void signUp() {
		ModelMapper modelMapper = new ModelMapper();
		UserService userService = new UserServiceImpl(userRepository, modelMapper, passwordEncoder, jwtService);
		userService.signUp(new SignUpDTO("Dmitry", "gavrilenko6f@gmail.com", "test12345"));
		verify(passwordEncoder, times(1)).encode(any(String.class));
		verify(userRepository, times(1)).save(any(User.class));
	}

	@Test
	public void signIn() throws UnsupportedEncodingException {
		UserService userService = Mockito.mock(UserServiceImpl.class);
		doThrow(AuthorizeException.class).when(userService);
		userService.signIn(new SignInDTO("gavrilenko6f@gmail.com", "test12345"));
	}
}