package com.dev.template.repository;

import com.dev.template.model.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@ActiveProfiles("test")
public class UserRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private UserRepository userRepository;

	@Before
	public void set() {
		userRepository.deleteAll();
	}

	@Test
	public void findUserByEmail() {
		entityManager.persist(new User("Dmitry",
				"gavrilenko6f@gmail.com", "test1234", "ROLE_USER"));
		User user = userRepository.findUserByEmail("gavrilenko6f@gmail.com").get();
		Assert.assertNotNull(user);
		Assert.assertEquals(user.getName(), "Dmitry");
		Assert.assertEquals(user.getPassword(), "test1234");
		Assert.assertEquals(user.getAuthority(), "ROLE_USER");
	}
}