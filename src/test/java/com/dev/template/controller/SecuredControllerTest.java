package com.dev.template.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SecuredControllerTest {

	private MockMvc mvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	private String token = "eyJhbGciOiJIUzI1NiJ9.eyJleHAiOjE1NTMwMTkyMzIsImlkIjo0NCwibmFt" +
			"ZSI6IkRtaXRyeSIsImVtYWlsIjoiZG1pdHJ5LnYuZ2F2cmlsZW5rb0BnbWFpbC5jb20iLCJhdXRob3JpdHkiOiJ" +
			"ST0xFX1VTRVIifQ.nBLrlbg1RO35zNlvMYyHje51OsXpYY1wf8tjO6u4jQU\";\n";

	@Before
	public void set() {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void secure() throws Exception {
		mvc.perform(get("/secure/greeting").header("Authorization", token))
				.andExpect(content().string("Hello from secure endpoint"));
	}
}