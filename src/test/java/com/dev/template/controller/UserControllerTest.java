package com.dev.template.controller;

import com.dev.template.dto.SignInDTO;
import com.dev.template.dto.SignUpDTO;
import com.dev.template.repository.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserControllerTest {

	private MockMvc mvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private UserRepository userRepository;

	private final SignUpDTO signUpDTO = new SignUpDTO("Dmitry",
			"gavrilenkotest@gmail.com", "test12345");

	private final SignInDTO signInDTO = new SignInDTO("gavrilenkotest@gmail.com", "test12345");

	@Before
	public void set(){
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@After
	public void clear() {
		userRepository.deleteAll();
	}

	@Test
	public void signAUp() throws Exception {
		mvc.perform(post("/user/sign-up")
				.content(objectMapper.writeValueAsBytes(signUpDTO))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().json("{" +
						"\"status\":\"OK\"," +
						" \"message\":\"User successfully created\"," +
						"\"code\":200}"));
	}

	@Test
	public void signBIn() throws Exception {
		signAUp();
		mvc.perform(post("/user/sign-in")
				.content(objectMapper.writeValueAsBytes(signInDTO))
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}
}