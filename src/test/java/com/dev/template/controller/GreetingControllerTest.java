package com.dev.template.controller;

import com.dev.template.service.JwtService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest({GreetingController.class, JwtService.class})
@ActiveProfiles("test")
public class GreetingControllerTest {

	@Autowired
	private MockMvc mvc;


	@Test
	public void greeting() throws Exception {
		mvc.perform(get("/greeting").accept(MediaType.TEXT_PLAIN))
				.andExpect(status().isOk())
				.andExpect(content().json("{status:OK, message:Hello, code:200}"));
	}
}