package com.dev.template.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/secure")
public class SecuredController {

	@GetMapping("/greeting")
	public ResponseEntity secure() {
		return ResponseEntity
				.status(HttpStatus.OK)
				.contentType(MediaType.TEXT_PLAIN)
				.body("Hello from secure endpoint");
	}

}
