package com.dev.template.controller;

import com.dev.template.dto.ResponseDTO;
import com.dev.template.exception.AuthorizeException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class HandlerController {
	@ExceptionHandler(AuthorizeException.class)
	public ResponseEntity authorizeException(AuthorizeException e){
		return ResponseEntity
				.status(e.getStatus())
				.contentType(MediaType.APPLICATION_JSON)
				.body(new ResponseDTO(e.getStatus(), e.getMsg(), e.getCode()));
	}
}
