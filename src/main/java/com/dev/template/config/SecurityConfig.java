package com.dev.template.config;

import com.dev.template.filter.JwtFilter;
import com.dev.template.provider.JwtProvider;
import com.dev.template.service.JwtService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.Filter;
import java.util.Collections;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private final JwtService jwtService;

	public SecurityConfig(JwtService jwtService) {
		this.jwtService = jwtService;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.cors().disable()
				.csrf().disable()
				.authorizeRequests()
				.antMatchers("/user/sign-up", "/user/sign-in").permitAll()
				.antMatchers("/secure/**").authenticated()
				.and()
				.httpBasic().disable()
				.addFilterBefore(filter(), UsernamePasswordAuthenticationFilter.class);
	}

	@Bean
	@Override
	public AuthenticationManager authenticationManager() {
		return new ProviderManager(Collections.singletonList(authenticationProvider()));
	}

	@Bean
	public Filter filter() {
		JwtFilter filter = new JwtFilter("/secure/**");
		filter.setAuthenticationManager(authenticationManager());
		filter.setAuthenticationSuccessHandler((req, res, auth) -> {});
		return filter;
	}

	@Bean
	public AuthenticationProvider authenticationProvider() {
		return new JwtProvider(jwtService);
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}

