package com.dev.template.service;

import com.dev.template.dto.ResponseDTO;
import com.dev.template.dto.SignInDTO;
import com.dev.template.dto.SignUpDTO;
import com.dev.template.dto.TokenDTO;

import java.io.UnsupportedEncodingException;

public interface UserService {

	ResponseDTO signUp(SignUpDTO signUpDTO);
	TokenDTO signIn(SignInDTO signInDTO) throws UnsupportedEncodingException;

}
