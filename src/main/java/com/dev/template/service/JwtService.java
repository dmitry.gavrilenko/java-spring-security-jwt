package com.dev.template.service;

import com.dev.template.details.UserDetailsImpl;
import com.dev.template.model.User;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.Calendar;

@Service
public class JwtService {

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expired}")
    private int expired;

    public String generateToken(User user) throws UnsupportedEncodingException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, expired);
        return Jwts.builder().setExpiration(calendar.getTime())
                .signWith(SignatureAlgorithm.HS256, secret.getBytes("UTF-8"))
                .claim("id", user.getId())
                .claim("name", user.getName())
                .claim("email", user.getEmail())
                .claim("authority", user.getAuthority())
                .compact();
    }

    public UserDetails getUserDetails(String token) {
        Jws<Claims> claims = parseToken(token);
        User user = new User();
        user.setId(claims.getBody().get("id", Long.class));
        user.setName(claims.getBody().get("name", String.class));
        user.setEmail(claims.getBody().get("email", String.class));
        user.setAuthority(claims.getBody().get("authority", String.class));
        return new UserDetailsImpl(user);
    }

    private Jws<Claims> parseToken(String token) {
        Jws<Claims> claims;
        try {
            claims = Jwts.parser().setSigningKey(secret.getBytes("UTF-8")).parseClaimsJws(token);
        } catch (Exception e) {
            throw new AuthenticationCredentialsNotFoundException("Cant parse token", e);
        }
        return claims;
    }
}
