package com.dev.template.service;

import com.dev.template.dto.ResponseDTO;
import com.dev.template.dto.SignInDTO;
import com.dev.template.dto.SignUpDTO;
import com.dev.template.dto.TokenDTO;
import com.dev.template.exception.AuthorizeException;
import com.dev.template.model.User;
import com.dev.template.repository.UserRepository;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException	;

@Service
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;

	private final  ModelMapper modelMapper;

	private final PasswordEncoder passwordEncoder;

	private final JwtService jwtService;

	public UserServiceImpl(UserRepository userRepository, ModelMapper modelMapper, PasswordEncoder passwordEncoder,
						   JwtService jwtService) {
		this.userRepository = userRepository;
		this.modelMapper = modelMapper;
		this.passwordEncoder = passwordEncoder;
		this.jwtService = jwtService;
	}

	@Override
	public ResponseDTO signUp(SignUpDTO signUpDTO) {
		User user = modelMapper.map(signUpDTO, User.class);
		user.setPassword(passwordEncoder.encode(signUpDTO.getPassword()));
		userRepository.save(user);
		return new ResponseDTO(HttpStatus.OK, "User successfully created", HttpStatus.OK.value());
	}

	@Override
	public TokenDTO signIn(SignInDTO signInDTO) throws UnsupportedEncodingException {
		User user = userRepository.findUserByEmail(signInDTO.getEmail()).orElseThrow(()
				-> new AuthorizeException(HttpStatus.BAD_REQUEST, "Wrong email or password", HttpStatus.BAD_REQUEST.value()));
		if (!passwordEncoder.matches(signInDTO.getPassword(), user.getPassword())) {
			throw new AuthorizeException(HttpStatus.BAD_REQUEST, "Wrong email or password", HttpStatus.BAD_REQUEST.value());
		}
		return new TokenDTO(jwtService.generateToken(user));
	}
}
