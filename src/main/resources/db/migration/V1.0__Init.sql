create table if not exists dev_user(
  id serial primary key ,
  name varchar(200),
  email varchar(200) unique,
  password varchar(200),
  authority varchar(20)
);